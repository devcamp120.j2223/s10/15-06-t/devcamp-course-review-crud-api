// Import course model vào controller
const courseModel = require("../models/courseModel");

// Khai báo thư viện mongoose 
const mongoose = require("mongoose");

const createCourse = (request, response) => {
    // B1: Thu thập dữ liệu
    let bodyRequest = request.body;

    // B2: Kiểm tra dữ liệu
    if(!bodyRequest.title) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Title is required"
        })
    }

    if(!(Number.isInteger(bodyRequest.student) && bodyRequest.student > 0)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "No Student is not valid"
        })
    }

    // B3: Thao tác với cơ sở dữ liệu
    let course = new courseModel({
        _id: mongoose.Types.ObjectId(),
        title: bodyRequest.title,
        description: bodyRequest.description,
        noStudent: bodyRequest.student
    });
    course.save((error) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(201).json({
                status: "Success: Course created",
                data: course
            })
        }
    });
    /*
    let createCourse = {
        _id: mongoose.Types.ObjectId(),
        title: bodyRequest.title,
        description: bodyRequest.description,
        noStudent: bodyRequest.student
    }

    courseModel.create(createCourse, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(201).json({
                status: "Success: Course created",
                data: data
            })
        }
    })*/

}

const getAllCourse = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let courseName = request.query.courseName;
    let minStudent = request.query.minStudent;
    let maxStudent = request.query.maxStudent;

    //B2: Validate dữ liệu (bỏ qua)
    //B3: Thao tác với cơ sở dữ liệu
    let condition = {};

    if (courseName) {
        //condition.title = courseName;
        condition.title = {$regex: courseName};
    }

    if (minStudent) {
        condition.noStudent = { $gte : minStudent };
    }

    if (maxStudent) {
        condition.noStudent = { ...condition.noStudent, $lte : maxStudent };
    }

    /*
    condition = {
        title:courseName, 
        $or : [
            {noStudent:{$lte : minStudent} }, 
            {noStudent: {$gte : maxStudent}}
        ]};*/
    console.log(condition);

    courseModel.find(condition, {'_id':0, 'title':1, 'noStudent' :1 }, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get courses success",
                data: data
            })
        }
    })    
}

const getCourseById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let courseId = request.params.courseId;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Course ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    courseModel.findById(courseId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get course success",
                data: data
            })
        }
    })
}

const updateCourseById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let courseId = request.params.courseId;
    let bodyRequest = request.body;

    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Course ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    let courseUpdate = {
        title: bodyRequest.title,
        description: bodyRequest.description,
        noStudent: bodyRequest.student
    }

    courseModel.findByIdAndUpdate(courseId, courseUpdate, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update course success",
                data: data
            })
        }
    })
}

const deleteCourseById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let courseId = request.params.courseId;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Course ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    courseModel.findByIdAndDelete(courseId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(204).json({
                status: "Success: Delete course success"
            })
        }
    })
}

// Export controller thành 1 module là 1 object gồm các hàm trong controller
module.exports = {
    createCourse: createCourse,
    getAllCourse: getAllCourse,
    getCourseById: getCourseById,
    updateCourseById: updateCourseById,
    deleteCourseById: deleteCourseById
}